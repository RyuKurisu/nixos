# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ ./hardware-configuration.nix		# Include the results of the hardware scan.
      ./desktop.nix
      #./phoenix.nix
      #./yggdrasil.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # Bootloader GRUB
  # boot.loader.grub.enable = true;
  # boot.loader.grub.devices = [ "/dev/sda" ];
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.useOSProber = true;

  # ZFS specific options 
  # boot.loader.grub.copyKernels = true;
  # boot.initrd.supportedFilesystems = [ "zfs" ]; # Not required if zfs is root-fs (extracted from filesystems) 
  # boot.supportedFilesystems = [ "zfs" ]; # Not required if zfs is root-fs (extracted from filesystems)
#  boot.zfs.requestEncryptionCredentials = true;
#  boot.kernelParams = [ "nohibernate" ];

  services.udev.extraRules = ''
  ACTION=="add|change", KERNEL=="sd[a-z]*[0-9]*|mmcblk[0-9]*p[0-9]*|nvme[0-9]*n[0-9]*p[0-9]*", ENV{ID_FS_TYPE}=="zfs_member", ATTR{../queue/scheduler}="none" ''; # ZFS already has its own scheduler, without this my(@Artturin) computer froze for a second when I Nix build something.

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  # Enable swap on luks
  boot.initrd.luks.devices."luks-eb971b17-f368-4487-89a8-9a2d779ffaa2".device = "/dev/disk/by-uuid/eb971b17-f368-4487-89a8-9a2d779ffaa2";
  boot.initrd.luks.devices."luks-eb971b17-f368-4487-89a8-9a2d779ffaa2".keyFile = "/crypto_keyfile.bin";

  fileSystems."/home/ryukurisu/SDcard" = { 
    device = "/dev/disk/by-label/SDcard";
    fsType = "ntfs";
  };

  networking.hostName = "phoenix"; 		# Define your hostname.
  networking.hostId = "50fecf1a";		# Register machine-id [head -c8 /etc/machine-id] (Required for reliable ZFS)

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  time.timeZone = "Europe/Amsterdam";		# Set your time zone.
  i18n.defaultLocale = "en_GB.utf8";		# Select internationalisation properties.

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "nl_NL.utf8";
    LC_IDENTIFICATION = "nl_NL.utf8";
    LC_MEASUREMENT = "nl_NL.utf8";
    LC_MONETARY = "nl_NL.utf8";
    LC_NAME = "nl_NL.utf8";
    LC_NUMERIC = "nl_NL.utf8";
    LC_PAPER = "nl_NL.utf8";
    LC_TELEPHONE = "nl_NL.utf8";
    LC_TIME = "nl_NL.utf8";
  };

  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkbOptions in tty.
  # };

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "altgr-intl";
  # xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    # media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ryukurisu = {
    isNormalUser = true;
    initialPassword = "pw123";
    description = "Christiaan Druif";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
      # vim_configurable
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # vim_configurable # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    duf
    htop
    inotify-tools
    killall
    syncthing
    tmux
    wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:
  networking.networkmanager.enable = true;	# Enable networkmanager.
  # networking.wireless.enable = true;		# Enable wireless support via wpa_supplicant.
  programs.nm-applet.enable = true;		# Enable network manager applet.
  services.teamviewer.enable = true;		# Enable Teamviewer.
  services.syncthing.enable = true;		# Enable Syncthing.
  services.flatpak.enable = true;		# Enable Flatpak.
  services.printing.enable = true;		# Enable CUPS to print documents.
  # services.openssh.enable = true;		# Enable the OpenSSH daemon.
  # services.zfs.autoScrub.enable = true;		# Enable ZFS automatic scrubbing.
  # services.zfs.autoSnapshot.enable = true;	# Enable ZFS automatic snapshotting.

  # systemd-services
  systemd.user.services.autorotate-sam = {
    enable = true;
    wantedBy = [ "default.target" ];
    description = "Enable Autorotate Screen and Mouse.";
    # path = [ /home/ryukurisu/.bin/autorotate-sam.sh ];
    after = [ "network-online.target" ];
    script = "/home/ryukurisu/.bin/autorotate-sam.sh";
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leavecatenate(variables, "bootdev", bootdev)
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
