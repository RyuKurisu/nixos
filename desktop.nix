{ config, pkgs, ... }:

{ 
  imports =
    [ ./kde.nix         # Everything needed for the Plasma Desktop.
      # .lxqt.nix
    ];

  # Enabling the X11 windowing system
  services.xserver.enable = true;
  
  # Enabling hardware components
  hardware.bluetooth.enable = true;             # Enable Bluetooth hardware.
  hardware.sensor.iio.enable = true;            # Enable iio-sensor-proxy.

  # Packages to add to the default selection
  environment.systemPackages = with pkgs; [ 
    alacritty
    bottles
    frescobaldi
    firefox
    gparted
    iio-sensor-proxy
    teamviewer 
  ];
}
